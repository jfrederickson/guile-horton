(hall-description
  (name "horton")
  (prefix "guile")
  (version "0.1")
  (author "Jonathan Frederickson")
  (copyright (2023))
  (synopsis "")
  (description "")
  (home-page "")
  (license gpl3+)
  (dependencies `(("goblins" ,guile-goblins)))
  (skip ())
  (files (libraries
           ((directory
              "horton"
              ((scheme-file "user-manager")
               (scheme-file "actor-sealers")
               (scheme-file "selfish-spawn")))))
         (tests ((directory
                   "tests"
                   ((scheme-file "test-user-manager")
                    (scheme-file "harness")))))
         (programs ((scheme-file "horton")))
         (documentation
           ((text-file "ChangeLog")
            (text-file "AUTHORS")
            (text-file "NEWS")
            (org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (text-file "INSTALL")
            (directory
              "doc"
              ((texi-file "horton")
               (info-file "guile-horton")
               (texi-file "version")
               (text-file "stamp-vti")
               (text-file ".dirstamp")))
            (directory
             "misc"
             ((scheme-file "user-manager-design")))))
         (infrastructure
           ((in-file "pre-inst-env")
            (automake-file "Makefile")
            (autoconf-file "configure")
            (directory
              "build-aux"
              ((tex-file "texinfo")
               (text-file "install-sh")
               (text-file "missing")
               (scheme-file "test-driver")
               (text-file "mdate-sh")))
            (scheme-file "guix")
            (text-file ".gitignore")
            (text-file ".envrc")
            (scheme-file "hall")))))
