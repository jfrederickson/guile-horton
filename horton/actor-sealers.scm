;;; Copyright 2020-2021 Christine Lemmer-Webber
;;; Copyright 2023 Jonathan Frederickson
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Basic Guile port of actor-sealers from the Racket version of
;;; Goblins. I'd contribute it upstream but it doesn't have contracts
;;; or tests like the Racket version, so I'm just keeping it here for
;;; now since I'll need it. :)

(define-module (horton actor-sealers)
  #:use-module (goblins)
  #:use-module (goblins utils simple-sealers)
  #:export (spawn-sealer-triplet))

(define* (spawn-sealer-triplet #:key (name #f))
  (define-values (seal unseal sealed?)
    ;; Guile warns that the arity might not be right. There must be a
    ;; better way to do this.
    (if name
        (make-sealer-triplet #:name name)
        (make-sealer-triplet)))

  (define (^sealed _bcom value)
    (define sealed-by-struct
      (seal value))
    (lambda () sealed-by-struct))

  (define (^sealer _bcom)
    (lambda (value)
      (spawn ^sealed value)))

  (define (^unsealer _bcom)
    (define (unseal-it sealed)
      (if (near-refr? sealed)
          (unseal ($ sealed))))
    (define (maybe-promise-to-unseal-it sealed)
      (if (and (near-refr? sealed) (local-object-refr? sealed))
          (unseal-it sealed)
          (on sealed unseal-it #:promise? #t)))
    maybe-promise-to-unseal-it)

  (define (^sealed? bcom)
    (lambda (sealed)
      (and (near-refr? sealed)
           (sealed? ($ sealed)))))

  (values (spawn ^sealer)
          (spawn ^unsealer)
          (spawn ^sealed?)))
