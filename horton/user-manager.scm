(define-module (horton user-manager)
  #:use-module (horton)
  #:use-module (goblins)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib common)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib ward)
  #:use-module (goblins ghash)
  #:use-module (srfi srfi-1)
  #:export (^user-manager))

(define (^user-manager _bcom)
  (define profiles->users (spawn ^ghash))
  (define-values (manager-warden manager-incanter) (spawn-warding-pair #:async? #t))
  (define public-methods
    (methods
     [(create-user username #:key (vat (spawn-vat)))
      (let ((new-user (make-principal username #:vat vat #:manager-warden manager-warden)))
        (on (<- new-user 'profile)
            (lambda (profile)
              ($ profiles->users 'set profile new-user)
              new-user)
            #:promise? #t))]
     [(list-users)
      (ghash-fold
       (lambda (k v prev)
         (cons k prev))
       '()
       ($ profiles->users 'data))]
     [(disable-user profile)
      (let ((do-disable (lambda (resolved-profile)
                          (let ((user ($ profiles->users 'ref resolved-profile)))
                            (cond
                             [(eq? user #f)
                              (error resolved-profile)
                              (error "User not found, could not disable! Are they managed by this user-manager?")]
                             [else
                              ;;(display ($ profiles->users 'data))
                              (<- manager-incanter user 'set-deny)])))))
        (cond
         [(promise-refr? profile)
          (on profile
              (lambda (resolved)
                (do-disable resolved))
              #:promise? #t)]
         [else
          (do-disable profile)]))]
     [(enable-user profile)
      (let ((user ($ profiles->users 'ref profile)))
        (<- manager-incanter user 'set-allow))]))
  public-methods)
