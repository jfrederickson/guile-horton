(define-module (horton)
  #:use-module (goblins)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib common)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib ward)
  #:use-module (goblins ghash)
  #:use-module (ice-9 match)
  #:use-module (horton actor-sealers)
  #:use-module (horton selfish-spawn)
  #:export (make-principal
            log-debug))

(define (log-debug str)
  "Temp hack to support the Racket-ported code below, replace with an
actual logger later"
  (display str)
  (display "\n"))

(define* (make-principal init-name #:key (vat (spawn-vat)) (manager-warden #f))
  (with-vat vat

            (define-values (my-seal my-unseal my-sealed?) (spawn-sealer-triplet))
            (define-values (my-warden my-incanter) (spawn-warding-pair))

            (define current-stubs (spawn ^ghash))

            (define (^profile _bcom principal)
              (methods
               [(name) ($ principal 'get-name)]
               ;; TODO: Make functions that expect to use the raw sealer object
               ;; use profiles instead
               [(sealer) ($ principal 'whoami)]))

            (define (^logger bcom)
              (define log
                (spawn ^cell '()))

              (methods
               [(append-to-log method args)
                (define new-log-entry
                  (list method args))
                (define current-log
                  ($ log))
                (define new-log
                  (cons new-log-entry current-log))
                ($ log new-log)]
               [(get-log)
                ($ log)]))

            (define logger (spawn ^logger))

            (define (^default-policy _bcom)
              (lambda () #t))
            (define (^deny-policy _bcom)
              (lambda () #f))
            (define default-policy (spawn ^default-policy))
            (define deny-policy (spawn ^deny-policy))
            (define principal-policy (spawn ^cell default-policy))

            (define (make-admin)
              (define (^admin bcom self)
                (define name (spawn ^cell init-name))
                (define my-profile (spawn ^profile self))
                (define public-methods
                  (methods
                   [(profile)
                    my-profile]
                   [(get-name) ($ name)]
                   [(set-name new-name)
                    ($ name new-name)]
                   [(new-proxy who-blame stub)
                    (make-proxy who-blame stub)]
                   [(new-stub who-blame targ)
                    (make-stub who-blame targ)]
                   [(whoami)
                    my-seal]
                   [(get-log)
                    ($ logger 'get-log)]
                   [(get-connections)
                    (ghash-for-each
                     (lambda (k v) k)
                     ($ current-stubs 'data))]
                   [(set-policy stub-id policy)
                    ($ my-incanter
                       ($ current-stubs 'ref stub-id)
                       'set-policy policy)]
                   [(get-current-stubs)
                    ($ current-stubs 'data)]))
                
                (define manager-methods
                  (methods
                   [(set-principal-policy new-policy)
                    ($ principal-policy new-policy)]
                   [(set-deny)
                    ($ principal-policy deny-policy)]
                   [(set-allow)
                    ($ principal-policy default-policy)]))
                (if manager-warden
                    (ward manager-warden manager-methods
                          #:extends
                          (lambda args
                            (if (and ($ principal-policy) ($ ($ principal-policy)))
                                (apply public-methods args)
                                (error "Request denied by policy")))
                          #:async? #t)
                    public-methods))
                   
              (selfish-spawn ^admin))

            (define (make-proxy whoBlame stub)
              ;;(define (log msg) (format "I ask ~a to: " ($ whoBlame
              ;; TODO: log stuff
              (define (do-intro arg)
                (cond
                 [(live-refr? arg)
                  (let* ((guts ($ my-incanter arg 'get-guts))
                         (s2 (car guts))
                         (objOwner (cadr guts))
                         (intro-result-vow
                          (<- s2 'intro whoBlame)))
                    (list "*proxy*" intro-result-vow objOwner))]
                 [else
                  (list "*primitive*" arg)]))
              (define (^proxy bcom self)
                ;; Two cases here. The first case, when someone calls 'get-guts,
                ;; just returns the guts of this proxy. This uses rights
                ;; amplification to ensure that only other objects in this
                ;; horton instance can call it.
                (define admin-methods
                  (methods
                   [(get-guts)
                    (list stub whoBlame)]))
                (define public-methods
                  (lambda xs
                    ;; In the second case, we want to pass any method calls to
                    ;; this proxy through to the proxy's stub.
                    (define processed-args (map do-intro xs))
                    (if ($ ($ principal-policy))
                        (begin
                          ($ logger 'append-to-log '*ask* `(,self ,xs))
                          (<- stub 'deliver processed-args))
                        (begin
                          ($ logger 'append-to-log '*request-denied* `('by ,who-blame 'on ,targ 'args ,processed-args))
                          (error "Request denied by policy")))))
                (ward my-warden admin-methods
                      #:extends public-methods))
              (selfish-spawn ^proxy))

            (define (make-stub who-blame targ)

              (define policy (spawn ^cell (spawn ^default-policy)))

              (define (^stub bcom self)
                (define (process-arg arg)
                  [match arg
                    ;; TODO: These tags should be symbols, not strings. I was
                    ;; just having trouble getting symbols to work when I wrote
                    ;; this
                    [("*proxy*" gift-proxy who-sealed)
                     (let ((new-stub ($ self 'unwrap gift-proxy who-sealed)))
                       (make-proxy who-sealed new-stub))]
                    [("*primitive*" val)
                     val]])

                (define admin-methods
                  (methods
                   [(set-policy new-policy)
                    ($ policy new-policy)]))

                (define public-methods
                  (methods
                   [(who-blame)
                    who-blame]
                   [(intro new-blame)
                    (log-debug "stub/intro: Received intro message")
                    ($ logger 'append-to-log '*intro* `('from ,who-blame 'what ,targ 'to ,new-blame))
                    (define new-stub
                      (let ((stub-id (list "*stub*" new-blame targ)))
                        ;; Check if we already have a stub for this
                        ;; (principal/target) pair. If we do, no need to create a
                        ;; new one - better to reuse the existing one for tracking
                        ;; and revocation purposes.
                        (cond [($ current-stubs 'has-key? stub-id)
                               (log-debug "stub/intro: Existing stub found, using that")
                               ($ current-stubs 'ref stub-id)]
                              ;; If we don't already have a stub for this
                              ;; (principal/target) pair, just create a new one.
                              [else
                               (make-stub new-blame targ)])))
                    (log-debug (format #f "stub/intro: Calling wrap on: ~a" self))
                    ($ self 'wrap new-stub new-blame)]

                   [(deliver args)
                    (define processed-args (map process-arg args))
                    (if (and ($ ($ policy)) ($ ($ principal-policy)))
                        (begin
                          ($ logger 'append-to-log '*request* `('by ,who-blame 'on ,targ 'args ,processed-args))
                          (apply <- (cons targ processed-args)))
                        (begin
                          ($ logger 'append-to-log '*request-denied* `('by ,who-blame 'on ,targ 'args ,processed-args))
                          (error "Request denied by policy")))]

                   ;; Wrap the new stub with the sealer of the object we're being
                   ;; introduced to. We can't use only sealers for this because that
                   ;; would fail in this case (from the Horton paper):
                   ;;
                   ;; - Alice sends Carol an (intro) message with Bob's sealer
                   ;; - Carol returns her stub sealed with Bob's sealer
                   ;; - Alice ignores this and sends Bob a completely different stub
                   ;; sealed with Bob's sealer
                   ;;
                   ;; Bob is then unable to tell that the sealer he got is actually
                   ;; from Carol. To avoid this, we need the handshake between Bob
                   ;; and Carol to involve both of their sealers.
                   [(wrap new-stub new-sealer)
                    ;; So we define a (provide) function that expects to receive
                    ;; another sealed function (actually a Goblins object) that only
                    ;; we can unseal...
                    (define (provide fill-box)
                      (log-debug "stub/wrap: Called (provide)")
                      (define fill ($ my-unseal fill-box))
                      ;; ...and uses this function to complete the handshake by
                      ;; sending the reference to the stub we've created for the new
                      ;; object.
                      (<- fill new-stub))
                    ;; But that'll come later. To start with, we're returning a
                    ;; sealed reference to the (provide) function that only the new
                    ;; contact can unseal.
                    ($ new-sealer (spawn (lambda _ provide)))]

                   ;; Now for the other side of the wrapping process. A wrapper
                   ;; needs an unwrapper, right?
                   [(unwrap gift new-sealer)
                    (log-debug "stub: Received unwrap message")
                    ;; The "wrapped" stub we get is actually a sealed actor
                    ;; reference that we need to call. So let's unseal it...
                    (define provide ($ my-unseal gift))
                    (log-debug "stub: Successfully unsealed provide function")
                    ;; ...and then because that actor expects to get an actor it can
                    ;; send the final stub to, let's create something to receive it.
                    ;; E's version of Horton set a variable and returned it, but
                    ;; given the typical asynchronous control flow of a Goblins
                    ;; program it feels easiest to have (unwrap) return a promise
                    ;; and have (fill) resolve that promise.
                    (define-values (stub-vow stub-resolver)
                      (spawn-promise-values))
                    (define (fill new-stub)
                      (log-debug "stub: Called (fill)")
                      ($ stub-resolver 'fulfill new-stub))
                    (<-np provide ($ new-sealer (spawn (lambda _ fill))))
                    stub-vow]))
                (ward my-warden admin-methods
                      #:extends public-methods))
              (let* ((stub-id (list "*stub*" who-blame targ))
                     (new-stub (selfish-spawn ^stub)))
                ($ current-stubs 'set stub-id new-stub)
                new-stub))
            (make-admin)))
