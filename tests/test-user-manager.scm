(define-module (tests test-user-manager)
  #:use-module (goblins)
  #:use-module (tests utils)
  #:use-module (horton user-manager)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-64))

(test-begin "test-user-manager")

(define mgr-vat (spawn-vat))
(define-vat-run mgr-run mgr-vat)

(define mgr (mgr-run (spawn ^user-manager)))

(define alice (mgr-run ($ mgr 'create-user "Alice")))
(define alice-profile (mgr-run (<- alice 'profile)))
(define bob (mgr-run ($ mgr 'create-user "Bob")))
(define bob-profile (mgr-run (<- bob 'profile)))

(define (^hello _bcom)
  (lambda ()
    "Hello world!"))

;; Arbitrary vat that Alice might be running objects in
(define alice-vat (spawn-vat))
(define-vat-run alice-run alice-vat)

;; Arbitrary vat that Bob might be running objects in
(define bob-vat (spawn-vat))
(define-vat-run bob-run bob-vat)

(define alice-hello (alice-run (spawn ^hello)))

(define stub-for-bob (alice-run (<- alice 'new-stub (<- bob-profile 'sealer) alice-hello)))

(define bob-proxy-to-alice (bob-run (<- bob 'new-proxy (<- alice-profile 'sealer) stub-for-bob)))

(let ((res (resolve-vow-and-return-result
            alice-vat
            (lambda () (<- bob-proxy-to-alice)))))
  (test-equal
      "Test that all this proxy communication works asynchronously"
    #(ok "Hello world!")
    res))

(define userlist (mgr-run ($ mgr 'list-users)))
(let ((alice-in-userlist? (resolve-vow-and-return-result
                           mgr-vat
                           (lambda () (car userlist))))
      (alice-profile-resolved (resolve-vow-and-return-result
                               mgr-vat
                               (lambda () alice-profile))))
  (test-equal
      "Test that the user list contains Alice's user profile like we expect
it to"
    alice-profile-resolved
    alice-in-userlist?))

(resolve-vow-and-return-result
            mgr-vat
            (lambda () (<- mgr 'disable-user alice-profile)))

(let ((res (resolve-vow-and-return-result
            alice-vat
            (lambda () (<- alice 'get-name)))))
  (test-assert
      "Test that methods on a user are denied when that user is disabled"
    (match res
      (#('err _err) #t)
      (_ #f))))

(let ((res (resolve-vow-and-return-result
            alice-vat
            (lambda () (<- bob-proxy-to-alice)))))
  (test-assert
      "Test that proxying to a user is denied when that user is disabled"
    (match res
      (#('err _err) #t)
      (_ #f))))

(test-end "test-user-manager")
