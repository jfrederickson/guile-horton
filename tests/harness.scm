(define-module (tests-harness)
  #:use-module (srfi srfi-64)
  #:use-module (goblins)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins ghash)
  #:use-module (tests utils)
  #:use-module (horton)
  #:use-module (horton actor-sealers)
  #:use-module (ice-9 match))

(module-define! (resolve-module '(srfi srfi-64))
		'test-log-to-file #f)

(test-begin "test-suite")

(define main-vat (spawn-vat))
(define-vat-run main-run main-vat)

(define (^hello bcom me)
  (methods
   [(say friend)
    (log-debug (format #f "Running 'say ~a" friend))
    (on (<- friend 'whoami)
        (lambda (ret)
          (format #f "Hello ~a, this is ~a!" ret me))
        #:promise? #t)]
   [(two-friends friend1 friend2)
    (on (<- friend1 'whoami)
        (lambda (f1)
          (on (<- friend2 'whoami)
              (lambda (f2)
                (format #f "Heard back from ~a and ~a!" f1 f2))
              #:promise? #t))
        #:promise? #t)]
   [(whoami)
    me]))

(define alice (main-run (spawn ^hello "Alice")))
(define bob (main-run (spawn ^hello "Bob")))
(define carol (main-run (spawn ^hello "Carol")))
(define eve (main-run (spawn ^hello "Eve")))

(define alice-admin (main-run (make-principal "Alice" #:vat main-vat)))
(define bob-admin (main-run (make-principal "Bob" #:vat main-vat)))
(define carol-admin (main-run (make-principal "Carol" #:vat main-vat)))
(define eve-admin (main-run (make-principal "Eve" #:vat main-vat)))
(define who-alice (main-run ($ alice-admin 'whoami)))
(define who-bob (main-run ($ bob-admin 'whoami)))
(define who-carol (main-run ($ carol-admin 'whoami)))
(define who-eve (main-run ($ eve-admin 'whoami)))

(define s1 (main-run ($ bob-admin 'new-stub who-alice bob)))
(define s2 (main-run ($ carol-admin 'new-stub who-alice carol)))
(define s3 (main-run ($ carol-admin 'new-stub who-bob carol)))
(define s4 (main-run ($ eve-admin 'new-stub who-alice eve)))

(define bob-current-stubs (main-run ($ bob-admin 'get-current-stubs)))
(test-equal
    "Test that Bob has correctly saved the new stub to his collection"
  s1
  (ghash-ref bob-current-stubs `("*stub*" ,who-alice ,bob)))

(test-equal
    "Test that we're able to get Alice's name from her profile"
  "Alice"
  (main-run ($ ($ alice-admin 'profile) 'name)))

(define (^fail-policy _bcom)
  (lambda () #f))
(define fail-policy (main-run (spawn ^fail-policy)))

;; Returns a wrapped version of Carol's s3 sealed with Bob's sealer
(define test-wrapped (main-run ($ s2 'wrap s3 who-bob)))

;; Now to test unwrapping it...
(main-run
 (on ($ s1 'unwrap test-wrapped who-carol)
     (lambda (ret)
       (test-equal
           "Test that Bob is able to unwrap Carol's S3"
         s3 ret))))

(define test-intro (main-run ($ s2 'intro who-bob)))
(define unwrapped-intro
  (main-run ($ s1 'unwrap test-intro who-carol)))

(test-equal
    "Test that the stub Bob received was meant for him"
  who-bob
  (main-run ($ unwrapped-intro 'who-blame)))

(main-run
 (on ($ unwrapped-intro 'deliver `(("*primitive*" ,'whoami)))
     (lambda (ret)
       (test-equal
           "Test that the stub Bob received goes to Carol"
         "Carol"
         ret))))

(define p1 (main-run ($ alice-admin 'new-proxy who-bob s1)))
(define p2 (main-run ($ alice-admin 'new-proxy who-carol s2)))
(define p4 (main-run ($ alice-admin 'new-proxy who-eve s4)))

(let ((ret (resolve-vow-and-return-result
            main-vat
            (lambda () (<- p1 'say p2)))))
  (test-equal
      "Test that Bob is able to reach Carol through the proxy"
    #(ok "Hello Carol, this is Bob!")
    ret))

(let ((ret (resolve-vow-and-return-result
            main-vat
            (lambda () (<- p1 'two-friends p2 p4)))))
  (test-equal
      "Test that Bob is able to reach both Carol and Eve through the proxy"
    #(ok "Heard back from Carol and Eve!")
    ret))

(define carol-current-stubs
  (main-run ($ carol-admin 'get-current-stubs)))

(define carol-stub-id (ghash-for-each (lambda (k v) k) carol-current-stubs))
(main-run ($ carol-admin 'set-policy carol-stub-id fail-policy))

(let ((res (resolve-vow-and-return-result
            main-vat
            (lambda () (<- p1 'two-friends p2 p4)))))
  (test-assert "Test that we throw an error when Carol has set the stub policy to fail"
    (match res
      [#('err problem) #t]
      [_ #f])))

;; Testing the flow above, but entirely async and in different vats
(define amanda-admin-vat (spawn-vat))
(define amanda-admin (main-run (make-principal "Amanda" #:vat amanda-admin-vat)))
(define ben-admin-vat (spawn-vat))
(define ben-admin (main-run (make-principal "Ben" #:vat ben-admin-vat)))

(define amanda-vat (spawn-vat))
;; Amanda wouldn't actually have a direct reference to ben-admin to
;; get the profile, this is just for simplification
(define ben-profile (with-vat amanda-vat (<- ben-admin 'profile)))
(define amanda (with-vat amanda-vat (spawn ^hello "Amanda")))
(define amanda-stub-for-ben (main-run (<- amanda-admin 'new-stub ben-profile amanda)))

(define ben-vat (spawn-vat))
(define amanda-profile (with-vat ben-vat (<- amanda-admin 'profile)))
(define ben-proxy-for-amanda (main-run (<- ben-admin 'new-proxy amanda-profile amanda-stub-for-ben)))

(let ((res (resolve-vow-and-return-result
            ben-vat
            (lambda () (<- ben-proxy-for-amanda 'whoami)))))
  (test-equal
      "Test that async proxy communication works"
    #(ok "Amanda")
    res))

(test-end "test-suite")
