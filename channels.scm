;; Deploy specific commits of my channels of interest.
(list (channel
       (name 'guix)
       (url "https://git.savannah.gnu.org/git/guix.git")
       (commit "428b810ca23fa1c1c565da15c0e95273f6487384")))
