(use-modules
 (goblins)
 (horton user-manager))

;; Optimistic design of user-manager

(define main-vat (spawn-vat))
(define-vat-run main-run main-vat)

(define user-manager (main-run (spawn ^user-manager)))
(define user-manager-root (main-run ($ user-manager 'add-service-user "User Management")))

(define alice-root (main-run ($ user-manager 'add-user "Alice")))

;; profiles should provide a way to get the "who" objects for each user (currently a sealer)
(define alice-profile (main-run ($ alice-root 'get-profile)))
(define who-alice (main-run ($ alice-profile 'get-who))) ;; TODO finalize method names
(define user-manager-profile (main-run ($ user-manager-root 'get-profile)))
(define who-user-manager (main-run ($ user-manager-profile 'get-who)))

;; Initial bootstrapping: alice is the first admin
(define user-manager-stub-alice (main-run ($ user-manager-root 'new-stub who-alice user-manager)))
(define user-manager-proxy-alice (main-run ($ alice-root 'new-proxy who-user-manager user-manager-stub-alice)))
;; TODO figure out a way to store caps per-user that's not a flat list
(main-run ($ alice-root 'add-cap user-manager-proxy-alice ($ user-manager-profile 'get-self-proposed-name))) ;; "User Management"

;; returns some sort of collection? needs to be human-readable somehow... probably petnames? how to store a collection efficiently? (alists? hashtables?)
;; open question: how do we handle collections of collections?
;; collections nested, petname db flat? if petname db is an alist, how to efficiently map everything from a collection to its petname?
;; doing that in a loop seems inefficient since alist lookup is O(n)
;; maybe use a hashtable for that reason
(define caps ($ alice-root 'get-caps))
(define machine1-admin (assq-ref caps "machine1-admin"))

(define machine1 (main-run ($ user-manager 'add-machine "machine1")))

($ bob-profile 'send machine1-admin)
