(use-modules
  (guix packages)
  ((guix licenses) #:prefix license:)
  (guix download)
  (guix build-system gnu)
  (gnu packages)
  (gnu packages autotools)
  (gnu packages gnupg)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages texinfo))

(define guile-goblins-patched
  (package
    (inherit guile-goblins)
    (propagated-inputs
     (list guile-fibers-1.3 guile-gcrypt))))

(package
  (name "guile-horton")
  (version "0.1")
  (source "./guile-horton-0.1.tar.gz")
  (build-system gnu-build-system)
  (arguments `())
  (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("pkg-config" ,pkg-config)
      ("texinfo" ,texinfo)))
  (inputs `(("guile" ,guile-3.0)))
  (propagated-inputs `(("goblins" ,guile-goblins-patched)))
  (synopsis "")
  (description "")
  (home-page "")
  (license license:gpl3+))

